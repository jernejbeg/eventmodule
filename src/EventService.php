<?php

namespace Drupal\eventmodule;

/**
 * Service for calculating event days
 */

class EventService{

    public function validateDate($event_start){
       
        $event_start = new \DateTime($event_start);
        $current_date = new \DateTime('today');

        $interval = date_diff($current_date, $event_start);
        $days = $interval->format('%d'); 

        if($days === '1'){
            return '1 day left until event starts';
        }
        elseif($days === '0'){
            return 'This event is happening today!';
        }
        else if ($event_start < $current_date){
            return 'This event already passed.';
        }
        else{
            return $days . ' days left until event starts';
        }
    }
}
