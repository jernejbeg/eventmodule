<?php

namespace Drupal\eventmodule\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a Date block
 * @Block(
 *   id = "eventmodule",
 *   admin_label = @Translation("Event Block"),
 * )
 */

 class EventBlock extends BlockBase{

    /**
     *  {@inheritdoc}
     */

     public function build(){
         return[
             '#markup' => $this->DaysLeft(),
             '#cache' => [
                 'max-age' => 0,
             ],
         ];
     }
     
     /**
      * Private function for calculating remaining days to event.
      */

     private function DaysLeft(){

        $nid = \Drupal::routeMatch()->getParameter('node')->id();
        $node_storage = \Drupal::entityTypeManager()->getStorage('node');
        $node = $node_storage->load($nid);
        $date = $node->get('field_event_date')->date;
        $event_service = \Drupal::service('eventmodule.event');
        return $event_service->validateDate($date);
     }

 }